public class BankAccount {

    private long balance;

    public BankAccount(long balance) {
        this.balance = balance;
    }

    public long balance() {
        return balance;
    }

    private static void transferFrom(BankAccount source, BankAccount dest, long amount) {
        source.balance -= amount;
        dest.balance += amount;
    }

    public static void main(String[] args) throws InterruptedException {
        BankAccount bugs = new BankAccount(100);
        BankAccount daffy = new BankAccount(100);

        final int N = 1000000;
        Thread bugsThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < N; i++)
                    transferFrom(daffy, bugs, 100);
            }
        });
        Thread daffyThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < N; i++)
                    transferFrom(bugs, daffy, 100);
            }
        });

        bugsThread.start();
        daffyThread.start();

        bugsThread.join();
        daffyThread.join();

        System.out.println(bugs.balance());
        System.out.println(daffy.balance());
    }
}
