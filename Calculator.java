public class Calculator {

    public static int add(int operand1, int operand2) {
        // write your code
    }

    public static int subtract(int operand1, int operand2) {
        // write your code
    }

    public static int multiple(int operand1, int operand2) {
        // write your code
    }

    public static float divide(int operand1, int operand2) {
        // write your code
    }

    public static void main(String[] args) {
        int addResult = add(4, 8);
        System.out.println("Add result: " + addResult);

        int subtractResult = subtract(12, 3);
        System.out.println("Subtract result: " + subtractResult);

        int multipleResult = multiple(5, 6);
        System.out.println("Multiple result: " + multipleResult);

        float divideResult = divide(16, 4);
        System.out.println("Divide result: " + divideResult);

        float invalidDivideResult = divide(16, 0);
        System.out.println("Divide result: " + invalidDivideResult);
    }
}
